# Copyriht 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

UPD72020X="uPD72020x-Firmware-master"

DESCRIPTION="Linux firmware files"
SRC_URI="https://github.com/denisandroid/uPD72020x-Firmware/archive/refs/heads/master.zip -> ${UPD72020X}.zip"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64"


src_unpack() {
	unpack ${UPD72020X}.zip
	mkdir -p ${P}
	mv ${UPD72020X}/UPDATE.mem ${P}/renesas_usb_fw.mem
}

src_install() {
	insinto /lib/firmware
	doins renesas_usb_fw.mem
}
