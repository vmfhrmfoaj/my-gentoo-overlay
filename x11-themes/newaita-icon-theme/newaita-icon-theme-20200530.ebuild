# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v

# cpoied from https://gpo.zugaina.org/x11-themes/newaita-icon-theme

EAPI=6

inherit git-r3 gnome2-utils

DESCRIPTION="Linux icon theme combining old style and color of material design."
HOMEPAGE="https://github.com/cbrnix/Newaita"
SRC_URI=""

EGIT_REPO_URI="https://github.com/cbrnix/Newaita"
EGIT_COMMIT="ca713b150c780a8a69c9d93bd4e9f6299c0bdf1e"
EGIT_CLONE_TYPE=shallow
KEYWORDS="amd64 x86"

RESTRICT="mirror"
LICENSE="CC-BY-NC-SA-3.0"
SLOT="0"
IUSE=""

src_prepare() {
	default
	rm -fv "${S}"/Newaita{,-dark}/{FV.sh,PV.sh,README.md,License-*} || die
}

src_install() {
	insinto "/usr/share/icons"
	doins -r Newaita{,-dark}

	dodoc README.md
}

pkg_preinst() { gnome2_icon_savelist; }
pkg_postinst() { gnome2_icon_cache_update; }
pkg_postrm() { gnome2_icon_cache_update; }
