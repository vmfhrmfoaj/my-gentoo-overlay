# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit font

COMMIT='d0516ddbcfa9746fcf533da7f2d1d07ca0f9fb07'
DESCRIPTION="Roboto Slab Typeface by Google"
HOMEPAGE="https://github.com/googlefonts/robotoslab"
SRC_URI="https://github.com/googlefonts/robotoslab/archive/${COMMIT}.zip -> ${P}.zip"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 arm arm64 x86"

DEPEND="app-arch/unzip"
RDEPEND=""

S="${WORKDIR}/robotoslab-${COMMIT}"
FONT_S="${S}/fonts/static"
FONT_SUFFIX="ttf"
DOCS="README.md"
