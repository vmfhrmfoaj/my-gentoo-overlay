# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit font

DESCRIPTION="An icon font for academics"
HOMEPAGE="https://jpswalsh.github.io/academicons/"
SRC_URI="https://github.com/jpswalsh/academicons/archive/v${PV}.zip -> ${P}.zip"

LICENSE="OFL 1.1"
SLOT="1"
KEYWORDS="amd64 arm arm64 x86"

DEPEND="app-arch/unzip"
RDEPEND=""

S="${WORKDIR}/academicons-${PV}"
FONT_S="${S}/fonts"
FONT_SUFFIX="ttf"
DOCS="README.md"
