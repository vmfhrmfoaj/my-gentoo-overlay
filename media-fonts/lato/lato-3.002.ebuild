# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit font

COMMIT='353ba57fb6a82049e1dc116d8180ebfbbefc1474'
DESCRIPTION="Lato is a sans serif typeface family started in the summer of 2010 by Warsaw-based designer Łukasz Dziedzic (“Lato” means “Summer” in Polish)."
HOMEPAGE="https://github.com/latofonts/lato-source"
SRC_URI="https://github.com/latofonts/lato-source/archive/${COMMIT}.zip -> ${P}.zip"

LICENSE="OFL 1.1"
SLOT="3"
KEYWORDS="amd64 arm arm64 x86"

DEPEND="app-arch/unzip"
RDEPEND=""

S="${WORKDIR}/lato-source-${COMMIT}"
FONT_S="${S}/fonts/static/TTF"
FONT_SUFFIX="ttf"
DOCS="README.md"
