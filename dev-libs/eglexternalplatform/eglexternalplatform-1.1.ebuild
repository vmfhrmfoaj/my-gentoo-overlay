# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="The EGL External Platform interface"
HOMEPAGE="https://github.com/NVIDIA/eglexternalplatform"
SRC_URI="https://github.com/NVIDIA/eglexternalplatform/archive/${PV}.tar.gz -> ${P}.tar.gz"

KEYWORDS="amd64 x86"

LICENSE="MIT"
SLOT="1"

src_install() {
	insinto /usr/include
	doins interface/eglexternalplatform.h
	doins interface/eglexternalplatformversion.h

	insinto /usr/share/pkgconfig
	doins eglexternalplatform.pc
}
