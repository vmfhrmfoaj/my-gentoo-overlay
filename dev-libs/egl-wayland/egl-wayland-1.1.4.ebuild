# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit autotools

DESCRIPTION="The EGLStream-based Wayland external platform"
HOMEPAGE="https://github.com/NVIDIA/egl-wayland"
SRC_URI="https://github.com/NVIDIA/egl-wayland/archive/${PV}.tar.gz -> ${P}.tar.gz"

KEYWORDS="amd64 x86"

LICENSE="MIT"
SLOT="1"

DEPEND="
	dev-libs/wayland
	dev-libs/eglexternalplatform
	media-libs/libglvnd
	>=x11-drivers/nvidia-drivers-396.24[-wayland]
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}"/${PV}-add-EGL_WL_bind_wayland_display-definitions-to-wayla.patch
)

src_configure() {
	autotools_run_tool ./autogen.sh
	econf
}
