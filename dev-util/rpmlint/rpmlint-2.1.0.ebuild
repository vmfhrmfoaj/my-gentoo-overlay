# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7


DISTUTILS_SINGLE_IMPL=1
PYTHON_COMPAT=( python3_{7,8,9} )

inherit distutils-r1

DESCRIPTION="Tool for checking common errors in RPM packages"
HOMEPAGE="https://github.com/rpm-software-management/rpmlint"
if [ "${PV}" = 9999 ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/rpm-software-management/rpmlint.git"
	KEYWORDS="~amd64"
else
	SRC_URI="https://github.com/rpm-software-management/rpmlint/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64"
fi

LICENSE="GPL-2"
SLOT="0"

DEPEND="
	${PYTHON_DEPS}
"
RDEPEND="
	app-arch/rpm[python,${PYTHON_SINGLE_USEDEP}]
	${PYTHON_DEPS}
"
