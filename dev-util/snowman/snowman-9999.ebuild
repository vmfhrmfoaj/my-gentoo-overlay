# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# - https://data.gpo.zugaina.org/soltys/app-forensics/snowman/snowman-9999.ebuild
# - https://github.com/yegord/snowman/blob/master/doc/build.asciidoc

EAPI=7

inherit git-r3 cmake

DESCRIPTION="A native code to C/C++ decompiler"
HOMEPAGE="https://derevenets.com/"
EGIT_REPO_URI="https://github.com/yegord/snowman.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtwidgets:5
"
RDEPEND="${DEPEND}"

CMAKE_USE_DIR="${S}/src"

src_configure() {
	local mycmakeargs=(
		-DBUILD_SHARED_LIBS=no
	)
	cmake_src_configure
}
