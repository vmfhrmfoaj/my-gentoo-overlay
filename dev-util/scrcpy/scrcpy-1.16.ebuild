# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# - original: https://data.gpo.zugaina.org/nelson-graca/app-mobilephone/scrcpy

EAPI=6

inherit meson ninja-utils

SRC_URI="https://github.com/Genymobile/scrcpy/archive/v${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/Genymobile/scrcpy/releases/download/v${PV}/scrcpy-server-v${PV} -> scrcpy-server-v${PV}.zip"

DESCRIPTION="Display and control your Android device"
HOMEPAGE="https://github.com/Genymobile/scrcpy"

LICENSE="Apache-2.0"
SLOT="1"
IUSE=""

RESTRICT="test"
KEYWORDS="amd64 x86"

COMMON_DEPEND="media-libs/libsdl2
	media-video/ffmpeg"

DEPEND="app-arch/unzip
	${COMMON_DEPEND}"

RDEPEND="${COMMON_DEPEND}"
PDEPEND=""

src_configure() {
	local emesonargs=(
		-Db_lto=true
		-Dprebuilt_server="${DISTDIR}/scrcpy-server-v${PV}.zip"
	)
	meson_src_configure
}
