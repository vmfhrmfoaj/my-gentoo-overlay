# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit gnome2-utils

DESCRIPTION="Draw on your screen in Gnome"
HOMEPAGE="https://codeberg.org/som/DrawOnYourScreen"
SRC_URI="https://codeberg.org/som/DrawOnYourScreen/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

RDEPEND="
	>=gnome-base/gnome-shell-3.34
"
DEPEND=""
BDEPEND=""

# NOTE
#  extension_uuid will be changed to 'draw-on-your-screen@som.codeberg.org'
#  see, https://codeberg.org/som/DrawOnYourScreen/issues/65
extension_uuid="drawOnYourScreen@abakkk.framagit.org"

src_unpack() {
	unpack "${P}".tar.gz
	mv "${WORKDIR}"/drawonyourscreen "${P}"
}

src_install() {
	einstalldocs

	insinto /usr/share/glib-2.0/schemas
	doins schemas/*.xml

	rm -rf schemas/ AUTHORS.md LICENSE Makefile README.md NEWS || die
	insinto /usr/share/gnome-shell/extensions/"${extension_uuid}"
	doins -r *
}

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_schemas_update
}
